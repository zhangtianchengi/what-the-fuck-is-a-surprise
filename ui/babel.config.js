const plugins = [
  [
    "import",
    {
      libraryName: "antd-mobile",
      style: "css"
    }
  ]
];
if (process.env.REACT_APP_BASE_URL === "production") {
  plugins.push("transform-remove-console");
}
module.exports = {
  presets: ["react-app"],
  plugins
};
