import axios from "axios";
const Axios = axios.create({
  baseURL:
    process.env.NODE_ENV === "production" ? "http://47.102.145.189:8009" : "",
  timeout: 10000
});
// 添加请求拦截器
Axios.interceptors.request.use(
  function (config) {
    // 在发送请求之前做些什么
    config.headers.authorization = window.sessionStorage.token
      ? window.sessionStorage.token
      : "";
    return config;
  },
  function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
  }
);

// 添加响应拦截器
Axios.interceptors.response.use(
  function (response) {
    // 对响应数据做点什么
    return response.data;
  },
  function (error) {
    // 对响应错误做点什么
    return Promise.reject(error);
  }
);
export default Axios;
