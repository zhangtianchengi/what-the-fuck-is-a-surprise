// 节流函数
export const throttle = (func) => {
  let timer;
  return function () {
    if (!timer) {
      timer = setTimeout(() => {
        timer = null;
        func.call(this, arguments);
      }, 1000);
    }
  };
};
// 防抖函数
export const debounce = (fn, wait) => {
  var timer = null;
  return function () {
    if (timer !== null) {
      clearTimeout(timer);
    }
    timer = setTimeout(fn, wait);
  };
};
