import { Redirect } from "react-router-dom";
const routerHOC = (item, routerConfig) => {
  if (item.isLogin) {
    const token = window.sessionStorage.token
      ? window.sessionStorage.token
      : "";
    if (!token) {
      document.title = item.meta.title;
      return <Redirect to='/login' exact></Redirect>;
    }
  }
  document.title = item.meta ? item.meta.title : "宏烨找房";
  return (
    <item.component {...routerConfig} routes={item.children}></item.component>
  );
};
export default routerHOC;
