import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import "@/assets/css/init.scss";
import zhCN from "zarm/lib/config-provider/locale/zh_CN";
import "zarm/dist/zarm.css";
import { ConfigProvider } from "zarm";
import { BrowserRouter } from "react-router-dom";
import ComponentDidCatch from "@/components/ComponentDidCatch/ComponentDidCatch.jsx";
import "./utils/setRem";
import vConsole from "vconsole";
// Icon
import "./assets/font/iconfont.css";
import { Provider } from "./store";
import "./pages/zwksb/iconfont.css";
// 在开发环境生成调试工具
if (process.env.NODE_ENV === "development") {
  new vConsole();
}
ReactDOM.render(
  <Provider>
    <ComponentDidCatch>
      <ConfigProvider locale={zhCN}>
        <BrowserRouter>
          <App />
        </BrowserRouter>
      </ConfigProvider>
    </ComponentDidCatch>
  </Provider>,
  document.getElementById("root")
);
