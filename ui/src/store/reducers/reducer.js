function reducer(state, active) {
  switch (active.type) {
    case "UPDATE_A":
      return {
        ...state,
        num: state.num + 1
      };

    default:
      return state;
  }
}

export default reducer;
