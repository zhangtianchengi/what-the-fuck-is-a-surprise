import React, { createContext, useReducer, useContext } from "react";
import reducer from "./reducers/reducer";

const myContext = createContext();
export const Provider = (props) => {
  const [state, dispatch] = useReducer(reducer, { num: 1 });
  return (
    <myContext.Provider value={{ state, dispatch }}>
      {props.children}
    </myContext.Provider>
  );
};
export const useStore = () => {
  const { state, dispatch } = useContext(myContext);
  return {
    state,
    dispatch
  };
};
