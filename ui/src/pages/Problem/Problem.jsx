import "./problem.scss";
import React, { Component } from "react";
import HeaderComponent from "@/components/HeaderComponent";
import { List, TextareaItem, InputItem } from "antd-mobile";
class Problem extends Component {
  state = {
    value: ""
  };
  onChange = (e) => {
    this.setState(
      {
        value: e.target.value
      },
      () => {
        console.log(e.target.value);
      }
    );
  };
  render() {
    return (
      <div className='problem'>
        <HeaderComponent
          title='问题反馈'
          back='block'
          goback={() => {
            this.props.history.push("/index/my");
          }}
        ></HeaderComponent>
        <div className='top-views'>
          <span className='spans'>联系方式</span>
        
          <InputItem
            {...('money3')}
            type='number'
           
            placeholder="请输入手机号"
            clear
            moneyKeyboardAlign="left"
        
          ></InputItem>
        </div>
        <div className='bottom'>
          <List renderHeader={() => "反馈内容"}>
            <TextareaItem rows={5} count={500} />
          </List>
        </div>

        <div className='btn'>提交反馈</div>
      </div>
    );
  }
}
export default Problem;
