import HeaderComponent from "@/components/HeaderComponent";
import "./Service.scss";
import { Icon, Button } from "antd-mobile";
const Service = (props) => {
  const list = [
    {
      num: "1",
      ty: "@@"
    },
    {
      num: "2",
      ty: "ABC"
    },
    {
      num: "3",
      ty: "DEF"
    },
    {
      num: "4",
      ty: "GHI"
    },
    {
      num: "5",
      ty: "JKL"
    },
    {
      num: "6",
      ty: "MNO"
    },
    {
      num: "7",
      ty: "PQRS"
    },
    {
      num: "8",
      ty: "TUV"
    },
    {
      num: "9",
      ty: "WXYZ"
    },
    {
      num: "*",
      ty: "WXYZ"
    },
    {
      num: "0",
      ty: "+"
    },
    {
      num: "*",
      ty: "W"
    }
  ];
  return (
    <div className='Service'>
      <HeaderComponent
        title='浏览记录'
        back='block'
        goback={() => {
          props.history.push("/index/my");
        }}
      ></HeaderComponent>
      <div className='service-di'>
        <p>422 155 7888</p>
        <p>上海</p>
      </div>
      <div className='service-bu'>
        <div className='bu-jia'>
          <div className='bs'>
            <p>
              <i className=' iconfont icon-svgadd'></i>
            </p>
            <p>新建联系人</p>
          </div>
          <div className='bs'>
            <p>
              <i className=' iconfont icon-lianxiren'></i>
            </p>
            <p>保存联系人</p>
          </div>
          <div className='bs'>
            <p>
              <i
                className=' iconfont icon-shipinhujiao
'
              ></i>
            </p>
            <p>视频呼叫</p>
          </div>
          <div className='bs'>
            <p>
              <i
                className=' iconfont icon-fasongxiaoxi
'
              ></i>
            </p>
            <p>发送信息</p>
          </div>
        </div>

        <div className='service-cc'>
          {list.map((item, index) => {
            return (
              <div key={index} className='enclish'>
                <p>{item.num}</p>
                <p>{item.ty}</p>
              </div>
            );
          })}
        </div>
        <div className='en'>
          <p className='ens'>
            <Icon type='cross-circle'></Icon>
          </p>
          <p className='ens'>
          <a href="tel:15535403948" >15535403948</a>
            <Button type='primary' size='small' inline onClick={()=>{
             
            }}>
              <i className=' iconfont icon-dianhua'></i> 中国联通
            </Button>
          </p>
          <p className='ens'>
            <Icon type='cross-circle-o'></Icon>
          </p>
        </div>
        <div className='ds'>
          <div className='cs'>
            <p>
              <i className=' iconfont icon-dianhua'></i>
            </p>
            <p>电话</p>
          </div>
          <div className='cs'>
            <p>
              <i className=' iconfont icon-lianxiren'></i>
            </p>
            <p>联系人</p>
          </div>
          <div className='cs'>
            <p>
              <i
                className=' iconfont icon-shoucang1
'
              ></i>
            </p>
            <p>收藏</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Service;
