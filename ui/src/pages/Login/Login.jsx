import { useState } from "react";
import { Toast } from "antd-mobile";
import { _login } from "@/api/api";
import handX from "../../images/8.png"; //背景
import "./login.scss";

const Login = (props) => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const handlesubmit = async () => {
    let res = await _login({
      username,
      password
    });

    if (res.status === 200) {
      Toast.success(res.description, 1);
      window.sessionStorage.setItem("token", res.body.token);
      props.history.replace("/index/home");
    } else {
      Toast.fail(res.description, 1);
    }
  };

  return (
    <div className='login'>
      <div className='login-wrapper'>
        <div className='cyan'>
          <img src={handX} alt='' className='imgs' />
          <h2>用心创造，创造未来</h2>
        </div>
        <div className='login-item'>
          <input
            type='text'
            className='form-control'
            placeholder='请输入用户名'
            value={username}
            onChange={(e) => {
              setUsername(e.target.value);
            }}
          />
        </div>
        <div className='login-item'>
          <input
            type='password'
            className='form-control'
            placeholder='请输入密码'
            value={password}
            onChange={(e) => {
              setPassword(e.target.value);
            }}
          />
        </div>
        <div className='login-item'>
          <button className='summit' onClick={handlesubmit}>
            登陆
          </button>
        </div>
      </div>
    </div>
  );
};

export default Login;
