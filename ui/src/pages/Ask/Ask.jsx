import { useEffect, useState, useRef, useCallback } from "react";
import { _userhouse_ } from "@/api/api";
import Select from "@/components/Select";
import HeaderComponent from "@/components/HeaderComponent";
import { Icon } from "antd-mobile";
import "./ask.scss";
const Ask = (props) => {
  const [list, setList] = useState([]);
  const _Oask = useRef(null);
  const selectlist = [
    {
      value: 0,
      label: "二手房出售"
    },
    {
      value: 1,
      label: "出租房"
    },
    {
      value: 2,
      label: "求购"
    },
    {
      value: 3,
      label: "求租"
    }
  ];
  const passlist = [
    {
      value: 0,
      label: "审核通过"
    },
    {
      value: 1,
      label: "审核未通过"
    }
  ];
  const userHouse = async () => {
    const res = await _userhouse_();
    if (res.code === 1) {
      setList(res.list);
    }
    // if (res.status === 200) {
    //   setList(res.body);
    // }
  };
  useEffect(() => {
    userHouse();
  }, []);
  const screen = (selectlist) => {
    console.log(selectlist);
  };
  return (
    <div className='ask' ref={_Oask}>
      <HeaderComponent
        title='我的发布'
        back='block'
        goback={() => {
          props.history.push("/index/my");
        }}
      ></HeaderComponent>
      <div className='tops'>
        <div className='dropdown'>
          <Select
            list={selectlist}
            width='100px'
            height='40px'
            getCity={() => screen(selectlist)}
          ></Select>
        </div>
        <div className='dropdown'>
          <Select
            list={passlist}
            width='100px'
            height='40px'
            getCity={() => screen(selectlist)}
          ></Select>
        </div>
      </div>
      <div className='list'>
        {list &&
          list.map((item, index) => {
            return (
              <dl key={index}>
                <dt>
                  <img src={item.img} alt='' />
                </dt>
                <dd>
                  <div className='cyans'>
                    <div className='p2'>{item.title}</div>
                    <div className='p3'>{item.status_name}</div>
                  </div>
                  <p style={{ fontSize: 13 }}>{item.areaname}</p>
                  <p>
                    {item.build_type.map((item, index) => {
                      return (
                        <span key={index} className='span2'>
                          {item}
                        </span>
                      );
                    })}
                  </p>
                  <p className='ps'>
                    <span style={{ color: "red" }}>
                      ￥{item.price}
                      {item.price_unit}
                    </span>
                  </p>
                  <p>{item.address}</p>
                </dd>
              </dl>
            );
          })}
      </div>
      <button
        onClick={useCallback(() => {
          // console.log(_Oask.current.scrollTop);
          let timer;
          timer = setInterval(() => {
            _Oask.current.scrollTop -= 20;
            if (_Oask.current.scrollTop < 20) {
              clearInterval(timer);
            }
          }, 2);
        }, [])}
        className='scrollTop'
      >
        <Icon type='up'></Icon>
      </button>
    </div>
  );
};

export default Ask;
