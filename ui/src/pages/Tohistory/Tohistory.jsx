import { useEffect, useState,useRef, useCallback} from "react";
import { _Browse } from "@/api/api";
import HeaderComponent from "@/components/HeaderComponent";
import "./tohistory.scss";
import { Icon } from "antd-mobile";
const Tohistory = (props) => {
  const [list, setList] = useState([]);
  const _OscrollTop=useRef(null)
  const getList = async () => {
    const res = await _Browse();
    if (res.code === 1) {
      setList(res.list);
    }
  };
  useEffect(() => {
    getList();
  }, []);
  return (
    <div className='tohistory' ref={_OscrollTop}>
      <HeaderComponent
        title='浏览记录'
        back='block'
        goback={() => {
          props.history.push("/index/my");
        }}
      ></HeaderComponent>
      <div className='list'>
        {list &&
          list.map((item, index) => {
            return (
              <dl key={index}>
                <dt>
                  <img src={item.img}></img>
                </dt>
                <dd>
                  <h4>{item.title}</h4>
                  <p>
                    {item.catname} {item.community_name}
                  </p>
                  <p style={{ color: "orange" }}>￥{item.danjia}/月</p>
                  <p>
                    {item.label.map((item, index) => {
                      return (
                        <span className='spans' key={index}>
                          {item.name}
                        </span>
                      );
                    })}
                  </p>
                </dd>
              </dl>
            );
          })}
      </div>
      <button
        onClick={useCallback(() => {
          let timer;
          timer = setInterval(() => {
            _OscrollTop.current.scrollTop-= 20;
            if (_OscrollTop.current.scrollTop < 20) {
              clearInterval(timer);
            }
          }, 2);
        },[])}
        className='scrollTop'
      >
        <Icon type='up'></Icon>
      </button>
    </div>
  );
};

export default Tohistory;
