import React, { Component } from "react";
import HeaderComponent from "@/components/HeaderComponent";
import { Icon } from "antd-mobile";
import "./Information.scss";
class Information extends Component {
  //点击互动消息
  interaction = () => {
    this.props.history.push("/interaction");
  };
  //点击系统消息
  system = () => {
    this.props.history.push("/system");
  };
  render() {
    return (
      <div className='Information'>
        <HeaderComponent
          title='消息'
          back='block'
          goback={() => {
            this.props.history.push("/index/my");
          }}
        ></HeaderComponent>
        <div className='middle'>
          <div className='my-midddle'>
            <span>互动消息</span>
            <span onClick={this.interaction}>
              <Icon type='right'></Icon>
            </span>
          </div>
          <div className='my-midddle'>
            <span>系统消息</span>
            <span onClick={this.system}>
              <Icon type='right'></Icon>
            </span>
          </div>
        </div>
      </div>
    );
  }
}
export default Information;
