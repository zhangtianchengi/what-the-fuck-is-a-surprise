import React, { useEffect, useState } from "react";
import { _getSwiperImgs, _getCustomIndexHtml, _getFangList } from "@/api/api";
import "./home.scss";
import { SwiperSlide, Swiper } from "swiper/react";
import "swiper/swiper.scss";
import { Button, Toast, Icon } from "antd-mobile";
import BetterScroll from "@/components/BetterScroll/BetterScroll";
import House from "@/components/House/House";
import Ershoufang from "@/components/House/Ershoufang";
import { debounce } from "../../../utils/index";
import ImgLazyLoad from "@/components/LazyLoad/imgLazyLoad";

function Home(props) {
  const navList = [
    { title: "买房", to: "/issueDetail/2/我想买个房" },
    { title: "租房", to: "/issueDetail/1/有房出租" },
    { title: "求租", to: "/issueDetail/3/我想租个房" },
    { title: "求购", to: "/issueDetail/2/我想买个房" },
    { title: "地图找房", to: "/mapLookRoom" },
    { title: "咨询", to: "/service" },
    { title: "楼市圈", to: "/housering" },
    { title: "经纪人", to: "/agent" },
    { title: "团购看房", to: "" },
    { title: "直播看房", to: "" }
  ];
  const [swiperList, setSwiperList] = useState([]);
  const [ershoufangList, setErshoufangList] = useState([]);
  const [value, setValue] = useState("");
  const [data, setData] = useState(null);
  const [i, setI] = useState(0);
  const types = [
    {
      type: "ershoufang",
      setting: 25,
      url: "/yapi/wapi/index/infoData.html/ershoufang"
    },
    {
      type: "xinfang",
      setting: 25,
      url: "/yapi/wapi/index/infoData.html/xinfang"
    },
    {
      type: "zufang",
      setting: 25,
      url: "/yapi/wapi/index/infoData.html/zufang"
    },
    {
      type: "zixun",
      setting: 25,
      url: "/yapi/wapi/index/infoData.html/zixun"
    },
    {
      label: 14,
      type: "ershoufang",
      setting: 25,
      url: "/yapi/wapi/index/infoData.html/xinshangfangyuan"
    }
  ];
  // 获取轮播图数据
  const getSwiperImgs = async () => {
    const res = await _getSwiperImgs();
    if (res.status === 200) {
      setSwiperList(res.body);
    }
  };
  // 获取home也主要数据
  const getCustomIndexHtml = async () => {
    const res = await _getCustomIndexHtml();
    console.log(res);
    if (res.code === 1) setData(res);
  };
  // 获取数据
  const geFangList = async () => {
    const res = await _getFangList(types[i]);
    console.log(res.list);
    if (res.code === 1) {
      setErshoufangList(res.list);
    }
  };
  useEffect(() => {
    getSwiperImgs();
    getCustomIndexHtml();
    geFangList();
  }, [props]);
  const onPullingUp = debounce(() => {
    console.log(1);
  }, 2000);

  return (
    <BetterScroll
      className='homeBScroll'
      config={{
        scrollY: true,
        pullDownRefresh: true,
        pullUpLoad: true
      }}
      pullingDown={() => {
        Toast.loading("Loading...", 1, () => {
          window.location.reload();
        });
      }}
      pullingUp={() => {
        console.log(1);
      }}
    >
      <div className='home'>
        <div className='headSearch'>
          <div className='searchInp'>
            <Icon type='search' size='xs' />
            <input type='text' name='' placeholder='Search' />
          </div>
          <Button
            type='primary'
            onClick={() => props.history.push("/mapLookRoom")}
          >
            地图
          </Button>
        </div>
        <div className='container'>
          <Swiper
            className='swiperBanner'
            spaceBetween={50}
            slidesPerView={1}
            autoplay={true}
            loop={true}
          >
            {swiperList.length ? (
              swiperList.map((item) => (
                <SwiperSlide key={item.id}>
                  <ImgLazyLoad
                    src={"http://47.102.145.189:8009" + item.imgSrc}
                  />
                </SwiperSlide>
              ))
            ) : (
              <div>暂无轮播图数据！</div>
            )}
          </Swiper>
          <nav>
            {navList.map((item, index) => (
              <dl
                key={index}
                onClick={() =>
                  navList[index].to && props.history.push(navList[index].to)
                }
              >
                <dt>
                  <img
                    src={data ? data.navs.nav[index].ico : ""}
                    alt=''
                    key={index}
                  />
                </dt>
                <dd>{item.title}</dd>
              </dl>
            ))}
          </nav>
          <div className='bannerTitle'>
            <b>本月房价</b>
            <button>
              <Icon type='check-circle' />
              <span>楼市早报</span>
            </button>
          </div>
        </div>
        <div className='card'>
          <div className='card_t'>
            <div className='card_t_l'>
              <p>
                <b>{data && data.benyuefangjia.left.value} </b> 元/m²
              </p>
              <p>
                <span>{data && data.benyuefangjia.left.desc}</span>
              </p>
            </div>
            <div className='card_t_r'>
              <p>
                <b>{data && data.benyuefangjia.right.value} </b> 元/m²
              </p>
              <p>
                <span>{data && data.benyuefangjia.right.desc} </span>
              </p>
            </div>
          </div>
          <div className='card_b'>
            <Swiper
              className='swiperNews'
              spaceBetween={15}
              slidesPerView={2}
              direction='vertical'
              autoplay={true}
              observer={true}
              loop={true}
            >
              {data
                ? data.news.news.map((item) => (
                    <SwiperSlide key={item.id} className='newsItem'>
                      <span
                        style={{
                          color: "#ffffff",
                          fontSize: "14px",
                          background:
                            "linear-gradient(to right, #ec632b, #f1a13a)",
                          borderTopLeftRadius: " 10px",
                          borderBottomRightRadius: "10px",
                          padding: "0 3px"
                        }}
                      >
                        动态：
                      </span>
                      {item.title}
                    </SwiperSlide>
                  ))
                : ""}
            </Swiper>
          </div>
        </div>
        <div className='bannerTitle'>
          <b>{data && data.hotBuild.title}</b>
          <span>更多</span>
        </div>
        {data
          ? data.hotBuild.hotBuild.map((item) => (
              <House key={item.id} item={item}></House>
            ))
          : ""}
        <div className='bannerTitle'>
          <b>二手房</b>
        </div>
        {ershoufangList.length &&
          ershoufangList.map((item) => (
            <Ershoufang key={item.id} item={item}></Ershoufang>
          ))}
        <Button type='primary' style={{ width: "80%", margin: "20px auto" }}>
          查看更多
        </Button>
      </div>
    </BetterScroll>
  );
}

export default Home;
