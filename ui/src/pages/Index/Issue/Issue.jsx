import React, { Component } from "react";
import style from "./issue.module.scss";
import className from "classnames";
export default class Issue extends Component {
  state = {
    list: [
      {
        title: "二手房出售",
        img: "https://images.tengfangyun.com/images/icon/ershoufang.png?x-oss-process=style/w_220",
        des: "发布二手房出售信息"
      },
      {
        title: "有房出租",
        img: "https://images.tengfangyun.com/images/icon/zufang.png?x-oss-process=style/w_220",
        des: "发布房源出租信息"
      },
      {
        title: "我想买个房",
        img: "https://images.tengfangyun.com/images/icon/qiugou.png?x-oss-process=style/w_220",
        des: "发布求购意向信息"
      },
      {
        title: "我想租个房",
        img: "https://images.tengfangyun.com/images/icon/qiuzu.png?x-oss-process=style/w_220",
        des: "发布求组意向信息"
      },
      {
        title: "买新房 职业顾问帮您忙",
        img: "https://images.tengfangyun.com/images/icon/xinfang.png?x-oss-process=style/w_220",
        des: "提交购房意向顾问全程携带看"
      }
    ]
  };
  goDetail(index) {
    let title = this.state.list[index].title
    this.props.history.push(title==="买新房 职业顾问帮您忙" ? '/agent' : '/issueDetail/'+ index +"/"+ title)
  }
  render() {
    const { list } = this.state;
    return (
      <div className={className(style.wrapper)}>
        <div className={className(style.issue_top)}>
          <div className={className(style.issue_header)}>
            <span>发&nbsp;&nbsp;布</span>
          </div>
        </div>
        <div className={className(style.main)}>
          <div className={className(style.contnet)}>
            {list &&
              list.map((item, index) => {
                return (
                  <div
                    key={index}
                    className={className(style.list_item)}
                    onClick={() => this.goDetail(index)}
                  >
                    <img src={item.img} alt='' />
                    <div className={className(style.info)}>
                      <div className={className(style.title)}>{item.title}</div>
                      <div className={className(style.sub_title)}>
                        {item.des}
                      </div>
                    </div>
                    <div className={className(style.gt)}>&gt;</div>
                  </div>
                );
              })}
          </div>
        </div>
      </div>
    );
  }
}
