/* eslint-disable no-unused-vars */
import { useEffect, useState } from "react";
import { Button, Icon } from "antd-mobile";

import handX from "../../../images/a.png"; //头像
import { _user } from "@/api/api";

import "./my.scss";

const My = (props) => {
  const [userInfo, setUserInfo] = useState(null);
  const user = async () => {
    const res = await _user();
    if (res.status === 200) {
      setUserInfo(res.body);
    }
  };
  useEffect(() => {
    user();
    return () => {};
  }, []);
  const clones = () => {
    props.history.push("/login");
    window.sessionStorage.clear();
  };
  // 点击消息页面
  const news = () => {
    props.history.push("/news");
  };
  //点击跳转收藏页面
  const Collection = () => {
    props.history.push("/collection");
  };
  //点击跳转支付页面
  const Payment = () => {
    props.history.push("/payment");
  };
  //点击跳转反馈npm页面
  const Problem = () => {
    props.history.push("/problem");
  };
  //点击跳转客服页面
  const Service = () => {
    props.history.push("/service");
  };
  //点击跳转发布页面
  const Ask = () => {
    props.history.push("/ask");
  };
  //点击历史页面
  const Tohistory = () => {
    props.history.push("/tohistory");
  };
  //点击发送接口

  return (
    <div className='my'>
      <div className='main'>
        <div className='box'>
          <h3 className='h3s'>个人中心</h3>
          <div className='boxs'>
            <dl className='mytop'>
              <dt>
                <img className='myimg' src={handX} alt='' />
              </dt>
              <dd className='ds'>
                <span> {userInfo && userInfo.nickname}</span>
                <span>{userInfo && userInfo.phone}</span>
              </dd>
              <dd>
                <span className='spans' onClick={news}>
                  <i
                    className='iconfont icon-fasongxiaoxi
'
                  ></i>
                </span>
              </dd>
            </dl>
            <ul className='mybottom'>
              <li>
                <span style={{ color: "#45b97c" }}>
                  <i
                    className='iconfont icon-wodefabu
'
                  ></i>
                </span>
                <span onClick={Ask}>我的发布</span>
              </li>
              <li>
                <span style={{ color: "#fdb933" }}>
                  <i className='iconfont icon-shoucang1'></i>
                </span>
                <span onClick={Collection}>收藏</span>
              </li>
              <li>
                <span style={{ color: "#f3715c" }}>
                  <i className='iconfont icon-liulanlishi'></i>
                </span>
                <span onClick={Tohistory}>浏览历史</span>
              </li>
            </ul>
          </div>
        </div>

        <div className='cylist'>
          <div className='mylist'>
            <span>支付明细</span>
            <span onClick={Payment}>
              <Icon type='right'></Icon>
            </span>
          </div>
          <div className='myisue'>
            <span>问题反馈</span>
            <span onClick={Problem}>
              <Icon type='right'></Icon>
            </span>
          </div>
          <div className='myserve'>
            <span>联系客服</span>
            <span onClick={Service}>
              <Icon type='right'></Icon>
            </span>
          </div>
        </div>
      </div>
      <footer className='footer'>
        <Button type='primary' inline size='large' onClick={clones}>
          退出登录
        </Button>
      </footer>
    </div>
  );
};

export default My;
