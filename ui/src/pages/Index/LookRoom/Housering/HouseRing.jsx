import { useEffect, useState } from 'react'
import { _getHouseRing } from "../../../../api/api"
import {useHistory} from "react-router-dom"
import style from "./housering.module.scss"
import {Button} from "antd-mobile"

function HouseRing() {
    let history =useHistory()
    const [HoustRingList, setHouseRingList] = useState([])

    const getHouseRing = async () => {
        let res = await _getHouseRing()
        if (res.code === 1) {
            setHouseRingList(res.lists)
            console.log(res.lists)
        }
    }

    useEffect(() => {
        getHouseRing()
    }, [])

     // 返回上一级
     const Goback = () => {
        history.go(-1)
    }

    return (
        <div className={style.housering}>
            <div className={style.ringtop}>
                <p onClick={Goback}>{"<"}</p>
                <h2>楼市圈</h2>
                <p>o</p>    
            </div>
            <ul className={style.list}>
                {HoustRingList.map((item,index)=>{
                    return (
                     <li key={index}>
                     <div className={style.top}>
                         <div className={style.la}><img className={style.imgs} src={item.prelogo} alt="" /></div>
                         <div className={style.con}>   
                             <div className={style.con_top}>
                                 {item.cname}
                             </div>
                             <div className={style.con_bot}>
                                {item.adviser_level_name}
                             </div>
                           </div>
                           <div className={style.ra}>
                               <p>
                               <Button className={style.but} type="ghost" inline size="small" style={{ marginRight: '4px' }}>在线咨询</Button>
                          <Button className={style.but} type="ghost" inline size="small" style={{ marginRight: '4px' }}>拨打电话</Button>

                               </p>
                        
                           </div>
                     </div>
                     <div className={style.bot}>
                                     <div className={style.left}>
                                         <h2>8月</h2>
                                         <span>{item.ctime.slice(5,11)}</span>
                                     </div>
                                     <div className={style.right}>
                                         <div className={style.title}>
                                              {item.content}
                                         </div>
                                         <div className={style.imgMsg}>
                                          {
                                                item.attached.map((it,ind)=>{
                                                 return <img key={ind} className={style.imgPath} src={it.path} alt="" />
                                                 })
                                          }
                                         </div>
                                 </div>
                       </div>
                 </li>)

                })}
               
                
                
            </ul>
        </div>
    )
}
export default HouseRing

