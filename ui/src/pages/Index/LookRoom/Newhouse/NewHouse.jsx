import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom"
import "./newhouse.css"
import { _getHotCity, _getBuyhouse, _getparentCity, _getSonCity } from "../../../../api/api"
import { SearchBar, Icon, Button } from 'antd-mobile';
import House from "../../../../components/House/House"


function NewHouse() {

    const [cityList, setCityList] = useState([]) //顶部下拉框
    const [area, setArea] = useState('')  //顶部搜索框
    const [houseList, setHouseList] = useState([]) //新房房源列表
    const [flag, setflag] = useState(false) //遮罩层开关
    const [areaFlag, setAreaFlag] = useState(false) //区域的内容开关
    const [priceFlag, setpriceFlag] = useState(false) //价格的内容开关
    const priceList = ['不限', '≤10万', '20万-30万', '30万-50万', '50万-100万', '≥1000万']
    const [typeFlag,setTypeFlag] = useState(false) //户型的内容开关
    const buyHoustList=['不限','新房','二手房'] //买房选型
    const typeList=['不限','1室','2室','3室','3室+']
    const [screenFlag,setScreenFlag] = useState(false) //筛选的条件开关is
    const trueList=['不限','安选']  //验真情况
    const towordList=['东','西','南','北','南北']
    const [parentCityList, setparentCityList] = useState([]) //父级城市列表数据
    const [sonCityList, setSonCityList] = useState([])

    // 请求顶部下拉框 区域的数据
    const getCityList = async () => {
        const res = await _getHotCity()
        if (res.status === 200) {
            setCityList(res.body)
        }
    }
    // 弹层左侧父级城市
    const getparentCity = async () => {
        let res = await _getparentCity({ level: '1' })
        if (res.status === 200) {
            setparentCityList(res.body)
        }
    }
    useEffect(() => {
        getCityList()
        getHouseList()
        getparentCity()
    }, [])
    // 新房房源列表
    const getHouseList = async () => {
        const res = await _getBuyhouse()
        if (res.code === 1) {
            setHouseList(res.list)
        }
    }
    // 顶部输入框的内容
    const handleArea = (e) => {
        setArea(e)
    }
    // 点击地图找房
    const GoMapLookHouse = () => {
        history.push("/mapLookRoom")
    }
    // 点击区域下拉
    const showArea = () => {
        setflag(true)
        setAreaFlag(true)
    }
    // 点击价格下拉
    const showprice = () => {
        setflag(true)
        setpriceFlag(true)
    }
    // 点击户型下拉
    const showType=()=>{
        setflag(true)
        setTypeFlag(true)
    }
    // 点击筛选下拉
    const showScreen=()=>{
        setflag(true)
        setScreenFlag(true)
    }
    // 确定按钮
    const handleBtn = () => {
        setflag(false)
        setAreaFlag(false)
        setpriceFlag(false)
        setTypeFlag(false)
        setScreenFlag(false)
    }
    // 点击左侧父级城市获取value值
    const handleParentCity = async (value) => {
        let res = await _getSonCity({ id: value })
        if (res.status === 200) {
            setSonCityList(res.body)
        }
    }


    let history = useHistory()
    // 返回上一级
    const Goback = () => {
        history.go(-1)
    }

    return (
        <div className="newhouse">
            <div className="newtop">
                <p onClick={Goback}>{"<"}</p>
                <h2>买    房</h2>
                <p>o</p>
            </div>
            <div className="newmain">
                <div className="maintop">
                    <div>
                        <select>
                            {
                                cityList.map((item, index) => {
                                    return <option key={index} value={index}>{item.label}</option>
                                })
                            }
                        </select>
                    </div>
                    <div className="maininput">
                        <SearchBar placeholder="输入区域/商圈" value={area} onChange={handleArea} />
                    </div>
                    <h2 onClick={GoMapLookHouse}>地图找房</h2>
                </div>
                <div className="mainmain">
                    <div>
                        <p>
                            <i className="iconfont icon-icon_bangwozhaofang"></i>
                        </p>
                        <p>找商铺</p>
                    </div>
                    <div>
                        <p>
                            <i className="iconfont icon-icon_bangwozhaofang"></i>
                        </p>
                        <p>找写字楼</p>
                    </div>
                    <div>
                        <p>
                            <i className="iconfont icon-icon_bangwozhaofang"></i>
                        </p>
                        <p>找小区</p>
                    </div>
                </div>
                <div className="mainselect">
                    <span onClick={showArea}>区域<Icon type="down" /></span>
                    <span onClick={showprice}>价格<Icon type="down" /></span>
                    <span onClick={showType}>户型<Icon type="down" /></span>
                    <span onClick={showScreen}>筛选<Icon type="down" /></span>
                </div>
                {
                    flag ? <div className="showarea">
                        {
                            areaFlag ? <div className="showareamain">
                                <div className="showleft">
                                    {
                                        parentCityList && parentCityList.map(item => {
                                            return <p key={item.value} onClick={() => handleParentCity(item.value)} >{item.label}</p>
                                        })
                                    }
                                </div>
                                <div className="showright">
                                    {
                                        sonCityList && sonCityList.map(item => {
                                            return <p key={item.value}>{item.label}</p>
                                        })
                                    }
                                </div>
                            </div> :
                                priceFlag ? <div className="showprice">
                                    {
                                        priceList.map((item,index)=>{
                                            return <div key={index}>{item}</div>
                                        })
                                    }
                                </div>: 
                                typeFlag?<div className="showtype">
                                        <h2>买房选型</h2>
                                        {
                                            buyHoustList.map((item,index)=>{
                                                return <span key={index}>{item}</span>
                                            })
                                        }
                                        <h2>户型</h2>
                                        {
                                            typeList.map((item,index)=>{
                                                return <span key={index}>{item}</span>
                                            })
                                        }
                                    </div>:
                                    screenFlag?<div className="showscreen">
                                            <h2>验真情况</h2>
                                            {
                                                trueList.map((item,index)=>{
                                                    return <span key={index}>{item}</span>
                                                })
                                            }
                                            <h2>朝向</h2>
                                            {
                                                towordList.map((item,index)=>{
                                                    return <span key={index}>{item}</span>
                                                })
                                            }
                                    </div> 
                                    :<></>
                        }
                        <div className="showareabtn"><Button type="primary" onClick={handleBtn}>确定</Button></div>
                    </div> : <div className="mainhouse">
                        {
                            houseList.map(item => {
                                return <House key={item.id} item={item} />
                            })
                        }
                    </div>
                }


            </div>
        </div>
    )
}
export default NewHouse;