import React, { useState } from "react";
import { SearchBar, Button, WhiteSpace, Toast } from "antd-mobile";
import "./lookroom.css";
import { useHistory } from "react-router-dom";

function LookRoom() {
  const [name, setName] = useState("");
  const [phone, setPhone] = useState("");

  // 跳转路由
  let history = useHistory();

  // 意向城市的防抖
  const getCityValue = (fn) => {
    // 防抖
    let timer = null;
    return function (e) {
      if (timer) {
        clearTimeout(timer);
      }
      timer = setTimeout(() => {
        fn(e);
      }, 3000);
    };
  };

  const CityValue = (e) => {
    console.log(e);
  };

  const show = getCityValue(CityValue);

  // 输入的姓名
  const handlename = (e) => {
    setName(e);
  };
  // 输入的手机号
  const handlePhone = (e) => {
    setPhone(e);
  };
  // 发布需求
  const handlesubmit = () => {
    if (!name || !phone) {
      Toast.fail("填写信息不完整 请重新输入！！！");
    }
  };
  // 新房
  const goNewhouse = () => {
    history.push("/newhouse");
  };
  // 地图找房
  const GoMaplookhouse = () => {
    history.push("/mapLookRoom");
  };
  // 经纪人
  const Agent = () => {
    history.push("/agent");
  };
  // 楼市圈
  const Housering = () => {
    history.push("/housering");
  };

  return (
    <div className='look-room'>
      <div className='lookroom-top'>
        <img
          src='https://images.sqfcw.com/images/new_icon/find_banner2.png?x-oss-process=style/w_8001'
          alt=''
        />
      </div>
      <div className='lookroom-main'>
        <div className='lookroom-cont'>
          <div className='couttop'>
            <p className='help'>帮我找房</p>
            <div>
              <SearchBar
                placeholder='请输入意向城市、区域、意向楼盘'
                onChange={(e) => {
                  show(e);
                }}
              />
            </div>
            <div className='namephone'>
              <div>
                <SearchBar
                  placeholder='请输入姓名'
                  value={name}
                  onChange={handlename}
                />
              </div>
              <div>
                <SearchBar
                  placeholder='请输入手机号'
                  value={phone}
                  onChange={handlePhone}
                />
              </div>
            </div>
            <p className='privacy'>隐私保护已开启</p>
            <div>
              <Button type='warning' onClick={handlesubmit}>
                发布需求
              </Button>
              <WhiteSpace />
            </div>
            <p className='service'>当前有148位置业顾问为您服务</p>
          </div>
          <ul className='countmain'>
            {/* 新房 */}
            <li onClick={goNewhouse}>
              <div className='licount licountfirst'>
                <div className='licountone'>
                  <h2>新房</h2>
                  <p>热门新楼盘 {">"} </p>
                </div>
                <div className='licounttwo'>
                  <i className='iconfont icon-icon_bangwozhaofang'></i>
                </div>
              </div>
            </li>
            {/* 地图找房 */}
            <li onClick={GoMaplookhouse}>
              <div className='licount licountsecond'>
                <div className='licountone'>
                  <h2>地图找房</h2>
                  <p>查看附近好房源 {">"} </p>
                </div>
                <div className='licounttwo'>
                  <i className='iconfont icon-msg'></i>
                </div>
              </div>
            </li>
            {/* 经纪人 */}
            <li onClick={Agent}>
              <div className='licount licountthird'>
                <div className='licountone'>
                  <h2>经纪人</h2>
                  <p>优选置业顾问 {">"} </p>
                </div>
                <div className='licounttwo'>
                  <i className='iconfont icon-wode-copy'></i>
                </div>
              </div>
            </li>
            {/* 楼市圈 */}
            <li onClick={Housering}>
              <div className='licount licountfour'>
                <div className='licountone'>
                  <h2>楼市圈</h2>
                  <p>楼市最新动态 {">"} </p>
                </div>
                <div className='licounttwo'>
                  <i className='iconfont icon-msg'></i>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
}

export default LookRoom;
