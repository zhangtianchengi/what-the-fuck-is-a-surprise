import React, { useState, useEffect } from 'react'
import { _getAgentInfo, _getSecondary, _getRenthouse } from "../../../../../api/api"
import "./agentinfo.css"

function AgentInfo() {

    const [agentObj, setAgentObj] = useState({})  //经纪人个人信息
    const [agentList, setAgentList] = useState([])  //房子信息列表
    const [isflag, setIsflag] = useState(true)  //true为二手房  false租房

    // 经纪人个人信息
    const getAgentInfo = async () => {
        let res = await _getAgentInfo()
        if (res.code === 1) {
            setAgentObj(res.agent)
        }
    }
    // 二手房房源信息列表
    const getSecondary = async () => {
        let res = await _getSecondary()
        if (res.code === 1) {
            setAgentList(res.house)
            setIsflag(true)
        }
    }
    // 租房房源信息
    const getRenthouse = async () => {
        let res = await _getRenthouse()
        if (res.code === 1) {
            setAgentList(res.house)
            setIsflag(false)
        }
    }

    useEffect(() => {
        getAgentInfo()
        getSecondary()
    }, [])

    return <div className="AgentInfo">
        <div className="top">
            <div className="topleft">
                <p>{agentObj.tname}</p>
                <h4>{agentObj.cname}</h4>
                <h4>
                    <i className="iconfont icon-msg"></i>
                    {agentObj.tel}
                </h4>
                <h4>
                    <i className="iconfont icon-msg"></i>
                    {agentObj.address}
                </h4>
            </div>
            <div className="topright">
                <img src={agentObj.img} alt="" />
            </div>
        </div>
        <div className="main">
            <div className="maintop">
                <p>
                    <span onClick={getSecondary}>卖房</span>
                    <span onClick={getRenthouse}>租房</span>
                </p>
                <p>
                    房源：25
                </p>
            </div>
            <div className="mainmian">
                {
                    agentList.map(item => {
                        return <div className="maincont" key={item.id}>
                            <div className="left">
                                <img src={item.img} alt="" />
                            </div>
                            <div className="right">
                                <h2>{item.title}</h2>
                                <p>
                                    <span>{item.shi}室{item.ting}厅{item.wei}卫|{item.mianji}㎡</span>&emsp; <span>{item.cmname}</span>
                                </p>
                                { 
                                    isflag ? <div className="bottom">
                                    <span className="fangjia">{item.fangjia}</span>万&emsp;<span className="danjia">{item.danjia}元/㎡</span>&emsp;{item.begintime}
                                </div>
                                : <div className="bottom">
                                    <span className="fangjia">{item.zujin}</span>元/月&emsp;&emsp;&emsp;&emsp;{item.begintime}
                                </div>
                                }
                                
                            </div>
                        </div>
                    })
                }
            </div>
        </div>
    </div>
}

export default AgentInfo