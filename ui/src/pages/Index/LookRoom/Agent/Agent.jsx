import React, { useState, useEffect } from "react";
import { _getAgentMemberList } from "../../../../api/api"
import "./agent.css"
import { useHistory } from "react-router-dom"
import { SearchBar } from 'antd-mobile';


function Agent() {

    let history = useHistory()
    // const [AgentList, setAgentList] = useState([])  //所有经纪人的数据
    const [AgentfirstList, setAgentfirstList] = useState([]) //经纪人前三名的数据
    const [AgentlastList, setAgentlastList] = useState([]) //经纪人四名以后的数据
    const [agentman, setagent] = useState([''])     //搜索框

    const getAgentList = async () => {
        let res = await _getAgentMemberList()
        if (res.code === 1) {
            // 根据house_count进行排序
            for(let i=res.list.length-1;i>=0;i--){
                for(let j=0;j<i;j++){
                    if(Number(res.list[j+1].house_count)>Number(res.list[j].house_count)){
                        [res.list[j],res.list[j+1]]=[res.list[j+1],res.list[j]]
                    }
                }
            }

            setAgentfirstList(res.list.slice(0, 3))
            setAgentlastList(res.list.slice(4))
        }
    }
    // 返回上一级
    const Goback = () => {
        history.go(-1)
    }
    // 搜索经纪人输入框
    const handleagentman = (e) => {
        setagent(e)
    }
    // 点击进入经纪人个人信息
    const handleGoAgentInfo=()=>{
        history.push("/agentInfo")
    }

    useEffect(() => {
        getAgentList()
    }, [])

    return <div className="agent">
        <div className="agenttop">
            <p onClick={Goback}>{"<"}</p>
            <h2>经纪人</h2>
            <p>o</p>
        </div>
        <div className="agentmain">
            <SearchBar placeholder="请输入需要查询的经纪人" value={agentman} onChange={handleagentman} />
            <div className="agentrank">
                {
                    AgentfirstList.map((item, index) => {
                        return <div key={item.id}>
                            <p><img src={item.img} alt="" /></p>
                            <p className="cnoone">No.{index + 1}</p>
                            <p className="cname">{item.cname}</p>
                        </div>
                    })
                }
            </div>
            <div className="agentranktwo">
                {
                    AgentlastList.map((item, index) => {
                        return <div key={index} onClick={handleGoAgentInfo}>
                            <p className='towone'>{index + 4}</p>
                            <div className='twotwo'>
                                <p><img src={item.img} alt="" /></p>
                                <div>
                                    <p>{item.cname}</p>
                                    <p>{item.tname}</p>
                                </div>
                            </div>
                            <div className='twothree'>
                                <i className='iconfont icon-msg'></i>
                                <i className='iconfont icon-fabu'></i>
                            </div>
                        </div>
                    })
                }
            </div>
        </div>
    </div>
}

export default Agent;