import HeaderComponent from "@/components/HeaderComponent";
import { Icon } from "antd-mobile";
import hang from "../../images/预售查询.png";
import "./Payment.scss";
const Payment = (props) => {
  return (
    <div className='Payment'>
      <HeaderComponent
        title='支付明细'
        back='block'
        goback={() => {
          props.history.push("/index/my");
        }}
      ></HeaderComponent>
      <div className='parment-list'>
        <h2>
          2021年1月 <Icon type='down' style={{ marginTop: 3 }}></Icon>
        </h2>
        <p className='p'>支出￥1917.61</p>
      </div>
      <div className='interaction-lists'>
        <div className='cyan-lists'>
          <dl>
            <dt>
              <img src={hang} alt='' />
            </dt>
            <dd>
              <p>微信支付 宏烨找房</p>
              <p>1月11日 12： 16</p>
            </dd>
          </dl>
          <div className='hours'>-10:00</div>
        </div>
      </div>
      <div className='interaction-lists'>
        <div className='cyan-lists'>
          <dl>
            <dt>
              <img src={hang} alt='' />
            </dt>
            <dd>
              <p>微信支付 宏烨找房</p>
              <p>1月11日 12： 16</p>
            </dd>
          </dl>
          <div className='hours'>-10:00</div>
        </div>
      </div>
    </div>
  );
};

export default Payment;
