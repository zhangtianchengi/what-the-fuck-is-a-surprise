import { Icon } from "antd-mobile";
import HeaderComponent from "@/components/HeaderComponent";
import "./new.scss";
const News = (props) => {
 
  //点击互动消息
  const interaction=()=>{
    props.history.push("/interaction");
  }
  //点击系统消息
  const system=()=>{
    props.history.push("/system");
  }
  return (
    <div className='news'>
     <HeaderComponent title="消息" back='block'
        goback={() => {
          props.history.push("/index/my");
        }}></HeaderComponent>
      <div className='middle'>
        <div className='my-midddle'>
          <span>互动消息</span>
          <span onClick={interaction}>
            <Icon type='right'></Icon>
          </span>
        </div>
        <div className='my-midddle'>
          <span>系统消息</span>
          <span onClick={system}>
            <Icon type='right'></Icon>
          </span>
        </div>
      </div>
    </div>
  );
};

export default News;
