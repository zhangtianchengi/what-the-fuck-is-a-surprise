/* eslint-disable no-sequences */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";
import style from "./detail.module.scss";
import classNames from "classnames";
import { Select, Cell, Input, Checkbox, Button } from "zarm";

export default class IssueDetail extends Component {
  state = {
    title: "",
    id: null,
    url: [],
    form: {
      xiaoqu: "",
      forward: "",
      mainji: "",
      type: "",
      areaname: "",
      level: "",
      leveltotal: "",
      price: "",
      title: "",
      des: "",
      likename: ""
    },
    ForwardList: [
      {
        value: "1",
        label: "东"
      },
      {
        value: "2",
        label: "南"
      },

      {
        value: "3",
        label: "西"
      },
      {
        value: "4",
        label: "北"
      }
    ],
    HouseType: [
      {
        value: "1",
        label: "高层住宅"
      },
      {
        value: "2",
        label: "小高层住宅"
      },

      {
        value: "3",
        label: "商铺/门面房"
      },
      {
        value: "4",
        label: "厂房"
      },
      {
        value: "5",
        label: "写字楼"
      }
    ],
    AreaList: [
      {
        value: "1",
        label: "主城区"
      },
      {
        value: "2",
        label: "宿豫区"
      },

      {
        value: "3",
        label: "宿城新区"
      },
      {
        value: "4",
        label: "苏宿新区"
      },
      {
        value: "5",
        label: "开发区"
      },
      {
        value: "6",
        label: "湖滨新区"
      }
    ]
  };
  componentDidMount() {
    let url = JSON.parse(localStorage.getItem("url"));
    let area = localStorage.getItem("area");
    let { title, id } = this.props.match.params;
    this.setState({
      title,
      id,
      url,
      form: {
        ...this.state.form,
        xiaoqu: area ? area : "请选择小区"
      }
    });
  }
  //上传图片
  addImg() {
    this.props.history.push("/addimage");
  }

  //朝向
  onOk(selected) {
    let label = selected.map((item) => item.label).join("");
    this.setState({
      form: {
        ...this.state.form,
        forward: label
      }
    });
  }
  //类型
  onOktype(selected) {
    let label = selected.map((item) => item.label).join("");
    this.setState({
      form: {
        ...this.state.form,
        type: label
      }
    });
  }
  //区域
  onOkarea(selected) {
    let label = selected.map((item) => item.label).join("");
    this.setState({
      form: {
        ...this.state.form,
        areaname: label
      }
    });
  }
  //楼层
  changeLevel(e) {
    this.setState({
      form: {
        ...this.state.form,
        level: e.target.value
      }
    });
  }
  //总楼层
  changeLevelTotal(e) {
    this.setState({
      form: {
        ...this.state.form,
        leveltotal: e.target.value
      }
    });
  }
  //面积
  onchangMianji(value) {
    this.setState({
      form: {
        ...this.state.form,
        mainji: value
      }
    });
  }
  //售价
  onchangPrice(value) {
    this.setState({
      form: {
        ...this.state.form,
        price: value
      }
    });
  }
  //标题
  onchangTitle(value) {
    this.setState({
      form: {
        ...this.state.form,
        title: value
      }
    });
  }
  //描述
  onchangDes(value) {
    this.setState({
      form: {
        ...this.state.form,
        des: value
      }
    });
  }
  //联系人
  onchangName(value) {
    this.setState({
      form: {
        ...this.state.form,
        likename: value
      }
    });
  }
  //提交
  btn() {
    localStorage.clear();
    console.log(this.state.form);
    this.setState({
      form:{
        ...this.state.form,
        xiaoqu: "",
        forward: "",
        mainji: "",
        type: "",
        areaname: "",
        level: "",
        leveltotal: "",
        price: "",
        title: "",
        des: "",
        likename: ""
      }
    })
    this.props.history.push("/pay")
  }
  render() {
    let { title, ForwardList, HouseType, AreaList, form } = this.state;
    return (
      <div className={classNames(style.main)}>
        <div className={classNames(style.detail_top)}>
          <span onClick={() => this.props.history.go(-1)}>&lt;</span>
          <span>{title}</span>
          <span></span>
        </div>
        <div className={classNames(style.detail_money)}>
          <span>i</span>
          <span>发布本条信息将收取10.00元</span>
          <span></span>
        </div>
        <div className={classNames(style.detail_content)}>
          <div className={classNames(style.detail_add)}>
            {title === "二手房出售" || title === "有房出租" ? (
              <div
                className={classNames(style.add)}
                onClick={() => this.addImg()}
              >
                <div className={classNames(style.add_img)}>
                  <span>+</span>
                </div>
                <div className={classNames(style.title)}>
                  <div>上传照片</div>
                  <div>
                    只能上传房屋图片，不能含有文字、数字、网址、名片、水印等，所有类别图片总计20张。
                  </div>
                </div>
                <div className={classNames(style.detail_gt)}>&gt;</div>
              </div>
            ) : (
              ""
            )}
          </div>
          <div>
            <Cell title='小区'>
              <Input
                clearable
                type='text'
                placeholder='请输入'
                value={form.xiaoqu}
              />
              <b
                onClick={() => {
                  this.props.history.push("/searcharea");
                }}
              >
                &gt;
              </b>
            </Cell>
            <Cell title='房产类型'>
              <Select
                dataSource={HouseType}
                onOk={(selected) => {
                  this.onOktype(selected);
                }}
              />
            </Cell>
            <Cell title='区域'>
              <Select
                dataSource={AreaList}
                onOk={(selected) => {
                  this.onOkarea(selected);
                }}
              />
            </Cell>
            <Cell title='朝向'>
              <Select
                dataSource={ForwardList}
                onOk={(selected) => {
                  this.onOk(selected);
                }}
              />
            </Cell>
            <div className={style.floor}>
              <div className='floorLeft'>
                所在楼层{" "}
                <input
                  type='text'
                  value={form.level}
                  onChange={(e) => this.changeLevel(e)}
                  placeholder='请填写'
                />{" "}
                层
              </div>
              <div className={style.floorRight}>
                共{" "}
                <input
                  type='text'
                  value={form.leveltotal}
                  onChange={(e) => this.changeLevelTotal(e)}
                  placeholder='请填写'
                />{" "}
                层
              </div>
            </div>
            <Cell title='面积'>
              <Input
                clearable
                type='text'
                placeholder='请输入面积'
                value={form.mainji}
                onChange={(value) => this.onchangMianji(value)}
              />
              ㎡
            </Cell>
            <Cell title='售价'>
              <Input
                clearable
                type='text'
                placeholder='请输入售价'
                value={form.price}
                onChange={(value) => this.onchangPrice(value)}
              />
              万
            </Cell>
            <Cell
              description={
                <Checkbox.Group type='button' defaultValue={["0", "1"]}>
                  <Checkbox value='0'>小户型</Checkbox>
                  <Checkbox value='1'>有电梯</Checkbox>
                  <Checkbox value='2'>好教育</Checkbox>
                  <Checkbox value='4'>低总价</Checkbox>
                  <Checkbox value='5'>满两年</Checkbox>
                  <Checkbox value='6'>满五年</Checkbox>
                  <Checkbox value='7'>低首付</Checkbox>
                  <Checkbox value='8'>品质好</Checkbox>
                  <Checkbox value='9'>采光好</Checkbox>
                  <Checkbox value='10'>隔音好</Checkbox>
                </Checkbox.Group>
              }
            >
              标签
            </Cell>
            <Cell title='标题'>
              <Input
                clearable
                type='text'
                placeholder='请输入标题'
                value={form.title}
                onChange={(value) => this.onchangTitle(value)}
              />
            </Cell>
            <Cell title='描述'>
              <Input
                clearable
                type='text'
                placeholder='请输入描述'
                value={form.des}
                onChange={(value) => this.onchangDes(value)}
              />
            </Cell>
            <Cell title='联系人'>
              <Input
                clearable
                type='text'
                placeholder='梦想'
                value={form.likename}
                onChange={(value) => this.onchangName(value)}
              />
            </Cell>
            <Cell>
              <div className='agreement-box'>
                <Checkbox id='agreement' />
                <label htmlFor='agreement'>
                &emsp;阅读并同意
                  <a
                    href='/#'
                    onClick={(e) => {
                      e.preventDefault();
                      alert("agree it");
                    }}
                  >
                《宏烨找房网房源发布规则》
                  </a>
                </label>
              </div>
            </Cell>
            <div className={classNames(style.btn)}>
              <Button
                theme='danger'
                onClick={() => {
                  this.btn();
                }}
              >
                发布并支付
              </Button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
