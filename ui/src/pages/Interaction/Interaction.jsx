import HeaderComponent from "@/components/HeaderComponent";
import { _news } from "@/api/api";
import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import "./Interaction.scss";
const Interaction = (props) => {
  const route = useHistory();
  const [newlist, setnewList] = useState([]);

  const getlist = async () => {
    let res = await _news();
    if (res.code === 1) {
      setnewList(res.friends);
    }
  };
  const goChatDetail = (id) => {
    route.push(`/chatdetail/${id}`);
  };
  useEffect(() => {
    getlist();
  }, []);

  return (
    <div className='Interaction'>
      <HeaderComponent
        title='互动消息'
        back='block'
        goback={() => {
          props.history.push("/index/my");
        }}
      ></HeaderComponent>
      <div className='interaction-list'>
        {newlist &&
          newlist.map((item) => {
            return (
              <dl key={item.user_id} onClick={() => goChatDetail(item.user_id)}>
                <dt>
                  <img src={item.headimage} alt='' />
                </dt>
                <dd>
                  <p>
                    <span className='nickname'>{item.nickname}</span>
                    <span className='alias_name'>{item.alias_name}</span>{" "}
                    <span className='time'>{item.chat.time}</span>
                  </p>
                  <p>
                    <span>{item.levelup_time}</span>
                  </p>
                </dd>
              </dl>
            );
          })}
      </div>
    </div>
  );
};

export default Interaction;
