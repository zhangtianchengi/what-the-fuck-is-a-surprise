import "./system.scss";
import HeaderComponent from "@/components/HeaderComponent";
const system = (props) => {
  return (
    <div className='system'>
      <HeaderComponent
        title='系统消息'
        back='block'
        goback={() => {
          props.history.push("/index/my");
        }}
      ></HeaderComponent>
      <div className='system-list'>
        <p>
          <span>您好，您发布的二手房出售已经审核通过</span>{" "}
          <span className='system-span'>上午7:00</span>
        </p>
        <p className='ss'>点击查看</p>
      </div>
    </div>
  );
};

export default system;
