import React from "react";
import { Link } from "react-router-dom";
import classNames from "classnames"
import style from "./index.module.scss";
import lkc6 from "../../assets/image/lkc6.png";

const Pay = (props) => {
    const goback =()=>{
        props.history.go(-1)
    }
  return (
    <div className={classNames(style.pay)}>
      <div className={classNames(style.pay_top)}>
        <div className={style.fan} onClick={goback}>
          ＜
        </div>
        <b>支付</b>
        <p></p>
      </div>
      <div className={style.phone}>
        <img src={lkc6} alt='' className={style.over} />
      </div>
      <div className={classNames(style.order)}>
        <p>订单编号</p>
        <p>18111123435883</p>
      </div>
      <div className={classNames(style.order)}>
        <p>订单金额</p>
        <p>10.00元</p>
      </div>
      <div className={classNames(style.order)}>
        <p>支付方式</p>
        <p>微信</p>
      </div>
      <div className={classNames(style.order)}>
        <p>支付时间</p>
        <p>2021-8-23 17:00</p>
      </div>
      <div className={classNames(style.publish)}>
        <button className={classNames(style.look)}>查看发布</button>
        <Link to='/index/issue' className={style.goon}>
          继续发布
        </Link>
      </div>
    </div>
  );
};

export default Pay;
