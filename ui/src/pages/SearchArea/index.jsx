/* eslint-disable array-callback-return */
import React, { Component } from "react";
import classNames from "classnames";
import { _community } from "@/api/api";
import style from "./index.module.scss";
import { SearchBar } from "antd-mobile";
export default class SearcgArea extends Component {
  state = {
    value: "",
    areaList: [],
    searchList: []
  };
  async componentDidMount() {
    const res = await _community();
    if (res.status === 200) {
      this.setState({
        areaList: res.body
      });
    }
  }

  // fangdou(fn, time) {
  //   var timer;
  //   return (e) => {
  //     if (timer) {
  //       clearTimeout(timer);
  //     }
  //     timer = setTimeout(() => {
  //       fn.call(this, e);
  //     }, time);
  //   };
  // }
  // handlechange(e) {

  //   console.log(e)
  // }

  // a = this.fangdou(this.handlechange, 1000);
  onChange = (value) => {
    this.setState({ value });
  };
  //模糊搜索
  submit(value) {
    const { areaList } = this.state;
    let arr = areaList.filter((item) => {
      if (item.communityName.includes(value)) {
        return item.communityName;
      }
    });
    this.setState({
      searchList: arr
    });
  }
  //点击x
  onClear(value) {
    this.setState({
      value,
      searchList: []
    });
  }
  chickItem(item) {
    this.props.history.go(-1)
    window.localStorage.setItem("area", item.communityName);
  }
  render() {
    const { searchList } = this.state;
    return (
      <div className={classNames(style.wrapper)}>
        <div className={classNames(style.main)}>
          <div className={classNames(style.top)}>
            <div onClick={()=>{this.props.history.go(-1)}}>&lt;</div>
            <div>查找小区</div>
            <div></div>
          </div>
          <div className={classNames(style.search)}>
            <SearchBar
              value={this.state.value}
              placeholder='请输入小区名称'
              onSubmit={(value) => {
                this.submit(value);
              }}
              onClear={(value) => {
                this.onClear(value);
              }}
              onCancel={() => {
                this.setState({ searchList: [], value: "" });
              }}
              showCancelButton
              onChange={this.onChange}
            />
          </div>
          {searchList &&
            searchList.map((item, index) => {
              return (
                <div
                  className={classNames(style.item)}
                  key={index}
                  onClick={() => this.chickItem(item)}
                >
                  {item.communityName}
                </div>
              );
            })}
        </div>
      </div>
    );
  }
}
