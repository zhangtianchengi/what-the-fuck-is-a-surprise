import { useEffect, useState } from "react";
import { _collection } from "@/api/api";
import HeaderComponent from "@/components/HeaderComponent";
import { Icon } from "antd-mobile";
import "./collection.scss";
const Collection = (props) => {
  const [list, setList] = useState([]);
  const userHouse = async () => {
    const res = await _collection();
    if (res.status === 200) {
      setList(res.body);
    }
  };
  useEffect(() => {
    userHouse();
  }, []);

  return (
    <div className='collection' id='collection'>
      <HeaderComponent
        title='收藏'
        back='block'
        goback={() => {
          props.history.push("/index/my");
        }}
      ></HeaderComponent>
      <div className='list'>
        {list &&
          list.map((item, index) => {
            return (
              <dl key={index}>
                <dt>
                  <img
                    src='https://img1.baidu.com/it/u=3989824903,2738076898&fm=26&fmt=auto&gp=0.jpg'
                    alt=''
                  />
                </dt>
                <dd>
                  <h3>{item.title}</h3>
                  <p>{item.desc}</p>
                  <p style={{ color: "orange" }}>￥{item.price}</p>
                  <p>
                    {item.tags.map((item, index) => {
                      return (
                        <span key={index} className='span1'>
                          {item}
                        </span>
                      );
                    })}
                  </p>
                </dd>
              </dl>
            );
          })}
      </div>

      <button
        onClick={() => {
          let timer;
          timer = setInterval(() => {
            document.documentElement.scrollTop -= 20;
            if (document.documentElement.scrollTop < 20) {
              clearInterval(timer);
            }
          }, 2);
        }}
        className='scrollTop'
      >
        <Icon type='up'></Icon>
      </button>
    </div>
  );
};

export default Collection;
