/* eslint-disable array-callback-return */
import React from "react";
import style from "./add.module.scss";
import classNames from "classnames";
import {
  ImagePicker,
  WingBlank,
  SegmentedControl,
  Button,
  WhiteSpace
} from "antd-mobile";

class AddImage extends React.Component {
  state = {
    files: [],
    multiple: false
  };
  onChange = (files, type, index) => {
    this.setState({
      files
    });
  };
  onSegChange = (e) => {
    console.log(e);
    const index = e.nativeEvent.selectedSegmentIndex;
    this.setState({
      multiple: index === 1
    });
  };
  //点击完成
  btn() {
    let arr = [];
    this.state.files.map((item) => {
      arr.push(item.url);
    });
    localStorage.setItem("url", JSON.stringify(arr));
    this.props.history.go(-1);
  }
  render() {
    const { files } = this.state;
    return (
      <div className={classNames(style.wrapper)}>
        <div>
          <div className={classNames(style.top)}>
            <div onClick={() => this.props.history.go(-1)}>&lt;</div>
            <div>添加照片</div>
            <div></div>
          </div>
          <div className={classNames(style.main)}>
            <div className={classNames(style.content)}>
              <div className={classNames(style.des)}>
                <span>室内图/视频</span>
                <span>最多上传10张{files.length}/10</span>
              </div>
              <div className={classNames(style.addimg)}>
                <WingBlank>
                  <SegmentedControl
                    selectedIndex={this.state.multiple ? 1 : 0}
                    onChange={this.onSegChange}
                  />
                  <ImagePicker
                    files={files}
                    onChange={this.onChange}
                    onImageClick={(index, fs) => console.log(index, fs)}
                    selectable={files.length < 10}
                    multiple={this.state.multiple}
                  />
                </WingBlank>
              </div>
            </div>
          </div>
        </div>
        <div className={classNames(style.box)}></div>
        <div className={classNames(style.btn)}>
          <Button type='warning' onClick={() => this.btn()}>
            完成
          </Button>
          <WhiteSpace />
        </div>
      </div>
    );
  }
}
export default AddImage;
