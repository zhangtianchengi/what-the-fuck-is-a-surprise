import React, { Component } from "react";
import "./mapLookRoom.scss";
import GD_Map from "@/components/GD_Map/GD_Map";

import { NavBar, Icon, Tabs } from "antd-mobile";
import { _getBuildCondition } from "@/api/api";
class MapLookRoom extends Component {
  state = {
    tabs: [
      {
        title: "新房",
        sub: "1",
        nav: [
          {
            title: "全部区域",
            listType: "area"
          },
          {
            title: "价格",
            listType: "price"
          },
          { title: "距离", listType: "distance" },
          {
            title: "更多",
            listType: ["types", "status", "renovation", "progress"]
          }
        ]
      },
      {
        title: "二手房",
        sub: "2",
        nav: [
          {
            title: "全部区域",
            listType: "area"
          },
          { title: "距离", listType: "distance" }
        ]
      },
      {
        title: "出租房",
        sub: "3",
        nav: [
          {
            title: "全部区域",
            listType: "area"
          },
          { title: "距离", listType: "distance" }
        ]
      }
    ],
    list: [],
    localList: null,
    open: false,
    parentIndex: null,
    childrenIndex: null
  };
  async getBuildCondition() {
    const res = await _getBuildCondition();
    console.log(res);
    if (res.code === 1) {
      this.setState({
        list: res
      });
    }
  }
  componentDidMount() {
    this.getBuildCondition();
  }
  // 点击展开菜单
  openMenu(index, ind) {
    this.setState({
      parentIndex: index,
      childrenIndex: ind
    });
    const { tabs, list, localList } = this.state;
    const { listType } = tabs[index].nav[ind];

    let arr = [];
    if (Array.isArray(listType)) {
      arr = listType.map((item) => ({
        title: item,
        children: [...list[item]]
      }));
    } else {
      arr = list[listType];
    }
    if (JSON.stringify(localList) === JSON.stringify(arr)) {
      this.content.style.height = "0px";
      this.setState({
        open: false,
        localList: null
      });
    } else {
      this.content.style.height = "200px";
      this.setState({
        open: true,
        localList: [...arr]
      });
    }
  }
  clickOption(e) {
    // 此处存在代码问题
    this.state.tabs[this.state.parentIndex].nav[
      this.state.childrenIndex
    ].title = e.target.innerHTML;
    this.content.style.height = "0px";
    this.setState({
      open: false,
      localList: null
    });
  }
  render() {
    const { tabs, localList, open } = this.state;
    return (
      <div className='mapLookRoom'>
        <NavBar
          mode='light'
          icon={<Icon type='left' />}
          onLeftClick={() => this.props.history.replace("/index/home")}
        >
          <b style={{ color: "#333333" }}>地图找房</b>
        </NavBar>
        <div className='tab'>
          <Tabs
            tabs={this.state.tabs}
            initialPage={0}
            tabBarPosition='top'
            renderTab={(tab) => <span>{tab.title}</span>}
          >
            {tabs.map((item, index) => (
              <div key={index} className='tabsItem'>
                {item.nav.map((ite, ind) => (
                  <span key={ind} onClick={() => this.openMenu(index, ind)}>
                    {ite.title}
                  </span>
                ))}
              </div>
            ))}
          </Tabs>
        </div>
        <div className='lookRoomMap'>
          <GD_Map></GD_Map>

          <div className='content' ref={(dom) => (this.content = dom)}>
            {open &&
              localList.map((item, index) => {
                return item.title ? (
                  <div className='more' key={index}>
                    <b>{item.title}</b>
                    <div className='moreOption'>
                      {item.children.map((ite, ind) => (
                        <span key={ind} onClick={(e) => this.clickOption(e)}>
                          {ite.typename ||
                            ite.leixing ||
                            ite.jiaofang ||
                            ite.jindu}
                        </span>
                      ))}
                    </div>
                  </div>
                ) : (
                  <p key={index} onClick={(e) => this.clickOption(e)}>
                    {item.areaname || item.name}
                  </p>
                );
              })}
          </div>
          {this.state.open ? (
            <div
              className='mask'
              onClick={() => {
                this.content.style.height = "0px";
                this.setState({
                  open: !this.state.open,
                  localList: null
                });
              }}
            ></div>
          ) : (
            ""
          )}
        </div>
      </div>
    );
  }
}

export default MapLookRoom;
