import React from "react";
import houseScss from "./house.module.scss";
import { useHistory } from "react-router-dom";
function House(props) {
  let history = useHistory();
  return (
    <div
      className={houseScss.hotHouse}
      onClick={() => history.push(`/houseDetail?id=${props.item.id}`)}
    >
      <dl>
        <dt>
          <img src={props.item.img} alt='' />
        </dt>
        <dd>
          <p>
            <b>{props.item.title}</b>
            <span>{props.item.status_name}</span>
          </p>
          <p>
            <b>{props.item.build_price}</b>
            {props.item.price_unit}
          </p>
          <p>
            {props.item.build_type.map((ite, index) => (
              <span key={index}>{ite}</span>
            ))}
          </p>
          {props.item.discount ? (
            <p>
              <span>惠</span>
              <span>{props.item.discount}</span>
            </p>
          ) : (
            ""
          )}
        </dd>
      </dl>
    </div>
  );
}

export default House;
