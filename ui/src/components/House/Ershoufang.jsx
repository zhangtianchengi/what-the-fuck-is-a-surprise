import React from "react";
import houseScss from "./house.module.scss";
import { useHistory } from "react-router-dom";
function House(props) {
  let history = useHistory();
  return (
    <div
      className={houseScss.ershoufang}
      onClick={() => history.push(`/houseDetail?id=${props.item.id}`)}
    >
      <dl>
        <dt>
          <img src={props.item.img} alt='' />
        </dt>
        <dd>
          <p>
            <span>顶</span>
            <span>{props.item.title}</span>
          </p>
          <p>
            <span>
              {props.item.shi}室{props.item.ting}厅{props.item.wei}卫 |{" "}
              {props.item.mianji}m²
            </span>
            <span>{props.item.community_name}</span>
          </p>
          <p>
            {props.item.label.map((ite, ind) => (
              <span key={ind} style={{ color: ite.color }}>
                {ite.name}
              </span>
            ))}
          </p>
          <p>
            <span>
              <b>{props.item.fangjia}</b> 万 {props.item.danjia}元/m²
            </span>
            <span>{props.item.begintime}</span>
          </p>
        </dd>
      </dl>
    </div>
  );
}

export default House;
