import React, { useState } from "react";
import { NavLink } from "react-router-dom";
import classnames from "classnames";
import "./layout.scss";
function Layout(props) {
  const [footList] = useState([
    {
      title: "首页",
      icon: "icon-1",
      to: "/index/home"
    },
    {
      title: "找房",
      icon: "icon-icon_bangwozhaofang",
      to: "/index/lookRoom"
    },
    {
      title: "发布",
      to: "/index/issue",
      icon: "icon-fabu"
    },
    {
      title: "消息",
      icon: "icon-msg",
      to: "/index/information"
    },
    {
      title: "我的",
      icon: "icon-wode-copy",
      to: "/index/my"
    }
  ]);
  return (
    <div className='index'>
      <main>{props.children}</main>
      <footer>
        {footList.map((item, index) => (
          <NavLink to={item.to} key={index}>
            <i
              className={classnames("iconfont", {
                [item.icon]: true
              })}
            ></i>
            <span>{item.title}</span>
          </NavLink>
        ))}
      </footer>
    </div>
  );
}

export default Layout;
