import React from "react";
import LazyLoad from "react-lazyload";
function ImgLazyLoad(props) {
  return (
    <LazyLoad>
      <img src={props.src} alt='' />
    </LazyLoad>
  );
}

export default ImgLazyLoad;
