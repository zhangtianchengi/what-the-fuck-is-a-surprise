import React, { useEffect } from "react";
import BScroll from "better-scroll";
const BetterScroll = (props) => {
  useEffect(() => {
    let scroll = new BScroll("." + props.className, {
      ...props.config,
      observeDOM: true,
      click: true,
      preventDefault: true,
      probeType: 1
    });
    props.config.pullDownRefresh &&
      scroll.on("pullingDown", () => {
        props.pullingDown(scroll);
        scroll.refresh();
        scroll.finishPullDown();
      });
    props.config.pullUpLoad &&
      scroll.on("pullingUp", () => {
        props.pullingUp(scroll);
        scroll.finishPullUp();
      });
    return () => ({});
  }, [props]);
  return (
    <div
      className={props.className}
      style={{ width: "100%", height: "100%", overflow: "hidden" }}
    >
      {props.children}
    </div>
  );
};
export default BetterScroll;
