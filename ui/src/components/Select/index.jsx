import { Icon } from "antd-mobile";
import style from "./select.module.scss";
import { useState, useRef, useEffect } from "react";
const Select = ({ list, height, width, getCity }) => {
  const [title, settitle] = useState(list[0].label);
  const [show, setshow] = useState(false);
  const area = useRef(null);
  const [left, setleft] = useState("0px");
  const changeselect = (e) => {
    setshow(!show);
  };
  const changetit = (ite) => {
    settitle(ite.label);
    setshow(!show);
    getCity&&getCity(ite);
  };
  useEffect(() => {
    setleft(`-${area.current.offsetLeft}px`);
    console.log(window.innerWidth);
  }, []);
  return (
    <div ref={area} className={style.select} style={{ width, height }}>
      <div className={style.selectcon} onClick={(e) => changeselect(e)}>
        <span>{title}</span>
        <Icon type="down" size="sm"></Icon>
      </div>
      <div
        className={style.selectarea}
        style={{
          width: window.innerWidth + "px",
          display: show ? "block" : "none",
          left,
        }}
      >
        {list.map((ite) => {
          return (
            <span key={ite.value} onClick={() => changetit(ite)}>
              {ite.label}
            </span>
          );
        })}
      </div>
    </div>
  );
};

export default Select;
