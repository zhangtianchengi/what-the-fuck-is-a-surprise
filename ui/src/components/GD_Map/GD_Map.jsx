/* eslint-disable no-undef */
import React, { Component } from "react";
import "./GD_Map.scss";
class GD_Map extends Component {
  //获取用户所在城市信息
  showCityInfo() {
    var map = new AMap.Map("container", {
      resizeEnable: true,
      //   center: [116.397428, 39.90923],
      zoom: 1,
      viewMode: "3D" //使用3D视图
    });
    //实时路况图层
    var trafficLayer = new AMap.TileLayer.Traffic({
      zIndex: 10
    });
    map.add(trafficLayer); //添加图层到地图
    //实例化城市查询类
    var citysearch = new AMap.CitySearch();
    //自动获取用户IP，返回当前城市
    citysearch.getLocalCity(function (status, result) {
      if (status === "complete" && result.info === "OK") {
        if (result && result.city && result.bounds) {
          var cityinfo = result.city;
          var citybounds = result.bounds;
          console.log(cityinfo, "cityinfo");
          //地图显示当前城市
          map.setBounds(citybounds);
        }
      } else {
        console.log(result.info, "result.info");
      }
    });
  }
  componentDidMount() {
    this.showCityInfo();
  }
  render() {
    return <div id='container' className={this.props.className}></div>;
  }
}

export default GD_Map;
