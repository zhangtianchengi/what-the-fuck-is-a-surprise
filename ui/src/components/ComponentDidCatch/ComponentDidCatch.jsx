import { Component } from "react";
import * as Sentry from "@sentry/react";
import { Integrations } from "@sentry/tracing";
Sentry.init({
  dsn: "https://40c48c6c7c044f23bcd910fabab1c553@o959882.ingest.sentry.io/5908214",
  integrations: [new Integrations.BrowserTracing()],
  environment: process.env.NODE_ENV,
  // Set tracesSampleRate to 1.0 to capture 100%
  // of transactions for performance monitoring.
  // We recommend adjusting this value in production
  tracesSampleRate: 1.0
});
class ComponentDidCatch extends Component {
  componentDidCatch(err, errInfo) {
    console.log("componentDidCatch", err, errInfo);
  }
  render() {
    return this.props.children;
  }
}
export default ComponentDidCatch;
