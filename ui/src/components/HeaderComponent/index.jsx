import style from "./index.module.scss";
import { SearchBar } from "antd-mobile";

const HeaderComponent = ({
  title,
  align = "center",
  search = "none",
  height = "80px",
  back = "none",
  space = "0px",
  valign = true,
  goback = null,
  onFocus
}) => {
  return (
    <div className={style.header} style={{ height: height }}>
      <div
        className={style.headercon}
        style={{ justifyContent: valign ? "space-around" : "space-between" }}
      >
        <div
          className={style.header_tit}
          style={{ textAlign: align, letterSpacing: space }}
        >
          {title}
        </div>
        <div className={style.header_inp} style={{ display: search }}>
          <SearchBar
            className={style.search_box}
            placeholder='在这里输入区域/商圈'
          />
        </div>

        <span className={style.back} style={{ display: back }} onClick={goback}>
          &lt;
        </span>
      </div>
    </div>
  );
};

export default HeaderComponent;
