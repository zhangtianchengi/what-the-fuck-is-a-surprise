import request from "@/utils/axios.js";
// 登录页
export const _login = (params) => request.post("/api/user/login", params);
// 首页
export const _getSwiperImgs = () => request.get("/api/home/swiper");
export const _getCustomIndexHtml = () =>
  request.get("/yapi/wapi/index/customIndex.html");
export const _getFangList = (params) => request.get(params.url, { params });
// 热门城市
export const _getHotCity = () => request.get("/api/area/hot");
// 经纪人接口
export const _getAgentMemberList = (params) =>
  request.get("/yapi/agentMemberList");
//小区关键词查询
export const _community = () => request.get("/yapi/area/community");
// 经纪人个人信息
export const _getAgentInfo = () => request.get("/yapi/agentInfo");
// 二手房的信息
export const _getSecondary = () => request.get("/yapi/secondary");
// 租房的信息
export const _getRenthouse = () => request.get("/yapi/renthouse");
// 买房房源信息列表
export const _getBuyhouse = () => request.get("/yapi/buyhouse");
// 楼市圈的数据
export const _getHouseRing = () => request.get("/yapi/housering");
// 左侧父级城市列表数据
export const _getparentCity = (params) =>
  request.get("/api/area/city", { params });
// 右侧子级城市列表数据
export const _getSonCity = (params) => request.get("/api/area", { params });

//用户信息的接口
export const _user = () => request.get("/api/user");

//我的发布页面
export const _userhouse = () => request.get("/api/user/houses");
export const _userhouse_ = () => request.get("/yapi/fabulist");
//收藏页面
export const _collection = () => request.get("/api/user/favorites");

//浏览记录
export const _Browse = () => request.get("/yapi/liulan");
// 地图找房
export const _getBuildCondition = () =>
  request.get("/yapi/wapi/build/buildCondition");
//消息详情
export const _ChatDetail = () => request.get("/yapi/im/chatLog.html");
//收藏页面
export const _news = () => request.get("/yapi/news");
