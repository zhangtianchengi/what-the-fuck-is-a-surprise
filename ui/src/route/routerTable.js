import { lazy } from "react";
const routerTable = [
  {
    path: "/",
    redirect: "/index"
  },
  {
    path: "/index",
    name: "index",
    component: lazy(() => import("../pages/Index/Index")),
    children: [
      {
        path: "/index",
        redirect: "/index/home"
      },
      {
        path: "/index/home",
        name: "home",
        component: lazy(() => import("../pages/Index/Home/Home")),
        meta: {
          title: "首页"
        }
      },
      {
        // 找房
        path: "/index/lookRoom",
        name: "lookRoom",
        component: lazy(() => import("../pages/Index/LookRoom/LookRoom")),
        meta: {
          title: "找房"
        }
      },
      {
        path: "/index/issue",
        name: "issue",
        component: lazy(() => import("../pages/Index/Issue/Issue")),
        isLogin: true,
        meta: {
          title: "发布"
        }
      },
      {
        path: "/index/information",
        name: "information",
        component: lazy(() => import("../pages/Index/Information/Information")),
        isLogin: true,
        meta: {
          title: "消息"
        }
      },
      {
        path: "/index/my",
        name: "my",
        component: lazy(() => import("../pages/Index/My/My")),
        isLogin: true,
        meta: {
          title: "我的"
        }
      }
    ]
  },
  {
    path: "/login",
    name: "login",
    component: lazy(() => import("../pages/Login/Login")),
    meta: {
      title: "登录"
    }
  },
  // 地图看房
  {
    path: "/mapLookRoom",
    name: "mapLookRoom",
    component: lazy(() => import("../pages/MapLookRoom/MapLookRoom")),
    meta: {
      title: "地图找房"
    }
  },
  {
    path: "/issueDetail/:id/:title",
    name: "issueDetail",
    component: lazy(() => import("../pages/IssueDetail/index.jsx")),
    meta: {
      title: "发布详情"
    }
  },
  {
    path: "/searcharea",
    name: "searcharea",
    component: lazy(() => import("../pages/SearchArea/index.jsx")),
    meta: {
      title: "查找小区"
    }
  },
  // 跳转新房页面
  {
    path: "/newhouse",
    name: "newhouse",
    component: lazy(() => import("../pages/Index/LookRoom/Newhouse/NewHouse")),
    meta: {
      title: "买房"
    }
  },
  //经纪人
  {
    path: "/agent",
    name: "agent",
    component: lazy(() => import("../pages/Index/LookRoom/Agent/Agent")),
    meta: {
      title: "经纪人"
    }
  },
  // 经纪人个人信息
  {
    path: "/agentInfo",
    name: "agentInfo",
    component: lazy(() =>
      import("../pages/Index/LookRoom/Agent/AgentInfo/AgentInfo")
    ),
    meta: {
      title: "经纪人详情"
    }
  },
  //楼市圈
  {
    path: "/housering",
    name: "housering",
    component: lazy(() =>
      import("../pages/Index/LookRoom/Housering/HouseRing")
    ),
    meta: {
      title: "楼市圈"
    }
  },
  // 收藏页面
  {
    path: "/collection",
    name: "Collection",
    component: lazy(() => import("../../src/pages/Collection/Collection")),
    meta: {
      title: "收藏"
    }
  },
  // 支付明细
  {
    path: "/payment",
    name: "Payment",
    component: lazy(() => import("../../src/pages/Payment/Payment")),
    meta: {
      title: "支付明细"
    }
  },
  // 问题反馈
  {
    path: "/problem",
    name: "Problem",
    component: lazy(() => import("../../src/pages/Problem/Problem")),
    meta: {
      title: "问题反馈"
    }
  },
  // 联系客服
  {
    path: "/service",
    name: "Service",
    component: lazy(() => import("../../src/pages/Service/Service")),
    meta: {
      title: "联系客服"
    }
  },
  // 消息页面
  {
    path: "/news",
    name: "News",
    component: lazy(() => import("../../src/pages/News/News")),
    meta: {
      title: "新消息"
    }
  },
  //发布添加图片
  {
    path: "/addimage",
    name: "AddImage",
    component: lazy(() => import("../pages/AddImage/index.jsx")),
    meta: {
      title: "上传图片"
    }
  },
  //支付
  {
    path: "/pay",
    name: "Pay",
    component: lazy(() => import("../pages/Pay/index.jsx")),
    meta: {
      title: "支付"
    }
  },
  // 我的发布
  {
    path: "/ask",
    name: "Ask",
    meta: {
      title: "发布"
    },
    component: lazy(() => import("../../src/pages/Ask/Ask"))
  },
  // 历史记录
  {
    path: "/tohistory",
    name: "Tohistory",
    meta: {
      title: "历史记录"
    },
    component: lazy(() => import("../../src/pages/Tohistory/Tohistory"))
  },
  //互动消息
  {
    path: "/interaction",
    name: "Interaction",
    meta: {
      title: "互动消息"
    },
    component: lazy(() => import("../../src/pages/Interaction/Interaction"))
  },
  //系统消息
  {
    path: "/system",
    name: "System",
    meta: {
      title: "系统消息"
    },
    component: lazy(() => import("../../src/pages/System/System"))
  },
  //房屋详情页
  {
    path: "/houseDetail",
    name: "houseDetail",
    meta: {
      title: "房屋详情"
    },
    component: lazy(() => import("../pages/HouseDetail/HouseDetail"))
  },
  //消息详情
  {
    path: "/chatdetail/:id",
    name: "Chatdetail",
    meta: {
      title: "消息详情"
    },
    component: lazy(() => import("../../src/pages/Chatdetail"))
  }
];
export default routerTable;
