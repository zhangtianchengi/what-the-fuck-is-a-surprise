import React from "react";
import routerTable from "./routerTable";
import { Redirect, Route, Switch } from "react-router-dom";
import routerHOC from "../utils/routerHOC";
export default function RouterView(props) {
  const routes = props.routes || routerTable;
  return (
    <Switch>
      {routes.map((item) => {
        return item.component ? (
          <Route
            path={item.path}
            key={item.name}
            render={(routerConfig) => {
              return routerHOC(item, routerConfig);
            }}
          ></Route>
        ) : (
          <Redirect
            from={item.path}
            to={item.redirect}
            exact
            key={item.path}
          ></Redirect>
        );
      })}
    </Switch>
  );
}
